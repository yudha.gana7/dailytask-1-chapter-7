import Card from './components/Card';
import Header from './components/Header';
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';

function App() {
    return ( <
        div className = "App" >
        <
        Header title = "Hello Yudha Gana" / >
        <
        Card title = "Testing"
        description = "Lorem ipsum dolor sit amet"
        btnText = "Klik Untuk Melanjutkan"
        btnHref = "https://google.com"
        imgSrc = "https://placeimg.com/320/240/any"
        imgAlt = "Hello" /
        >
        </div>
    );
}

export default App;