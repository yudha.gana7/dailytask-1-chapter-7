import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import Header from './components/Header'
import MainLayout from "./layouts/MainLayout";
import Button from "./components/Button";

function State() {
    return ( <
        div className = "State mt-5" >
        <
        Header title = "Button Components" / >

        <
        MainLayout >
        <
        Button variant = "primary" >
        Klik Biru Muda </Button>

        <
        Button variant = "secondary" >
        Klik Putih </Button>

        <
        Button variant = "black" >
        Klik Hitam </Button> 
        </MainLayout > 
        </div>
    );
}

export default State;